
##**Data Visualization Using R, Shiny, and Tableau**
### **Final Project: Drug Abuse and Mental Health **##
####**Julianne Crea, Janel Chumley, and Stephen Rauner**


**This is our link to our Dashboard**

[Flex Dashboard Here](https://juliannecrea.shinyapps.io/flex/)

##**Data Source**
We downloaded our dataset from the CDC's website. The data comes from the National Health and Nutrition Examination Survey (NHANES), from the 2013-14 session. NHANES is designed to assess the health and nutritional status of adults and children in the U.S., and is a major program of the National Center for Health Statistics (NCHS). The survey examines a nationally representative sample of about 5,000 people each year; they are in counties across the country, 15 of which are visited each year. We downloaded six tables of data: responses to questions about alcohol use, demographics, drug use, income, mental health, and occupation. The dataset can be found [here](https://wwwn.cdc.gov/Nchs/Nhanes/Search/DataPage.aspx?Component=Demographics&CycleBeginYear=2013). 


##**Tableau Visualizations**

###**Bar Graph: Depression vs Income by Marital Status**
**Interpretation**

**Divorced**

The happiest divorced individuals are in **$75,000 - 99,000** income bracket with a **never depressed** rate of 90.91%. The runner-up for happy divorced people is the **$100,000 +** income bracket, with a **never depressed** rate of 88.37%. 
![depIncome_1.png](https://bitbucket.org/repo/X4GyL8/images/943066749-depIncome_1.png)

**Living with a partner**

Middle class income earners who are also living with a partner appear to be the happiest. 90% of subjects in the **$65,000 to 74,999** income bracket are **never depressed**. The runner-up is the **$100,000 +** income bracket with a **never depressed** rate of 83.33%. The unhappiest individuals in this category fall into the **$15,000 - 19,999** income bracket with a **never depressed** rate of 59.38%. 
![depIncome2.png](https://bitbucket.org/repo/X4GyL8/images/3433830148-depIncome2.png)

**Widowed**

Widowed middle-class earners in the **$45,000 to 54,999** income bracket are the happiest with a **never depressed** rate of 100%. The unhappiest widows are in the lowest income bracket, $0-4,999, with 100% of subjects being depressed for **several days**.  
![depIncome4.png](https://bitbucket.org/repo/X4GyL8/images/152961625-depIncome4.png)

**Separated**

Separated individuals in the **$100,000 +** bracket are the happiest with 100% of subjects never being depressed. Widowed individuals in the lowest income bracket are the unhappiest with only 25% of subjects never being depressed. 
![depIncome5.png](https://bitbucket.org/repo/X4GyL8/images/243042480-depIncome5.png)

**Never Married**

Single people are relatively happy across all income brackets. Interestingly, 78.79% of earners in the **$0-4,999** income bracket are never depressed, a figure higher than all other income brackets with the exception of the **$100,000 +** bracket which has a never depressted rate of 85.61%.  
![depIncome6.png](https://bitbucket.org/repo/X4GyL8/images/743269509-depIncome6.png)

**Married**

The happiest married people are in the highest income bracket of **$100,000 +** with a **never depressed** rate of 87.10%. Never depressed rates gradually decrease as income levels decrease between $75,000-99,999 and **$20,000-24,999**. Poorer married couples are relatively unhappy with **never depressed** rates between 62.50% and 69.57%.  
![depIncome7.png](https://bitbucket.org/repo/X4GyL8/images/2346706042-depIncome7.png)

**Steps to Create Multiple Inner Joins in Tableau**

Step 1: From the Data Source page, select MENTALHEALTH and drag-drop to **Drag tables here**. 

Step 2: Select ALCOHOL and drag-drop next to MENTALHEALTH to create an inner join. 

Step 3: Select DEMOGRAPHICS and drag-drop next to ALCOHOL to create another inner join. 

Step 4: Select DRUGUSE and drag-drop next to DEMOGRAPHICS to create another inner join. 

**Steps to Produce Bar Graph in Tableau**

Step 1: From the MENTALHEALTH table under dimensions, select Depressed and drag-drop onto the Columns shelf. From there right-click and choose filter and uncheck null. Then right-click again and choose Measure and Count. Right-click again and select Quick Table Calculation then Percent of Total. Finally, right-click again and select Edit Table Calculation. Choose Specific Dimension and select the checkbox Depressed. 

Step 2: From the DEMOGRAPHICS table under dimensions, select Annual HH Income and drag-drop onto the Rows shelf. Right-click and choose Sort and Descending.  

Step 3: From the MENTALHEALTH table under dimensions, select Depressed and drag-drop onto color in the marks shelf. 

Step 4: From the DEMOGRAPHICS table under dimensions, select Marital and drag-drop onto the Pages shelf. Right-click again and select Filter and uncheck Null. 

Step 5: On the upper right-hand side of the workspace window, click on the Show Me icon and select Bar Graph. 

Step 6: Above the Columns shelf, click on the Swap icon. 





###**Box Plot: Severe Depression v. Income Poverty Levels by Gender**
####**Interpretation**

**Female**

Female respondents who are depressed **nearly every day** and **more than half the days** have a median income to poverty ratio of 1 and 0.91 respectively. Females who feel severely depressed for **several days** have a median income to poverty ratio of 1.30. Notice that females who are **never** severely depressed have a median income to poverity ratio of 2.02.
![severeDep_1.png](https://bitbucket.org/repo/X4GyL8/images/3097401602-severeDep_1.png)

**Male**

For male respondents in this survey, those who feel severely depressed **nearly everyday** have a median income to poverty ratio of 0.63 while those who are **never** severely depressed have a median income to poverty ratio of 2.18. In general, for male respondents, as the severity of depression increases the income to poverty ratio descreases.
![severeDep_2.png](https://bitbucket.org/repo/X4GyL8/images/1992755484-severeDep_2.png)

**Steps to Produce Box Plot in Tableau**

Step 1: From the MENTAL HEALTH table under Dimensions select Better Dead then drag-drop onto the Columns shelf. Right-click and choose Sort, then select Manual. From there order the attributes as follows: Nearly every day, More than half the days, Several days, Never. 

Step 2: From the DEMOGRAPHICS table under Measures select Ratio Income Poverty and drag-drop onto the Rows shelf. 

Step 3: From the MENTAL HEALTH table under Dimensions select Better Dead then drag-drop onto the Color in the Marks shelf. 

Step 4: From the DEMOGRAPHICS table under Measures select Gender and drag-drop onto the Pages shelf. 

Step 5: On the upper right-hand side of the workspace window, click on the Show Me icon and select Box Plot.





###**Depression vs Savings by Monthly Income**
**Interpretation**

We clearly see that people with more savings don’t report feeling depressed. It is rare for those with $1000+ in savings to feel depressed for even a few days. Nearly all of the respondents who have felt depressed for at least several days have savings under $500.  Most larger circles are in this <$500 in savings area, so most of the respondents who have used a hard drug have little savings. Most circles here are red, orange, or yellow, so are on the lower half of the monthly income spectrum. However, we do have high earning individuals with little savings who feel depressed, as shown by the green circles. Few of these use hard drugs, though. Notice that the two highest earners (light blue) are in the “nearly every day” category for depression and don’t have very high savings (they have $501-1000). 
![depSavings.png](https://bitbucket.org/repo/X4GyL8/images/968234536-depSavings.png)

**Steps to Recreate**

Step 1: Go to "Data Source" and  drag the MENTALHEALTH table to the **Drag tables here** area. Drag DRUGUSE to the right of MENTALHEALTH. Drag INCOME under DRUGUSE. Make sure MENTALHEALTH is inner joined to both DRUGUSE and INCOME. 

Step 2: Go to Sheet 1 and rename it "Depression vs Savings". Uncheck aggregate measures. Drag the dimension "Savings"" to the columns shelf and dimension "Depressed"" to the rows shelf. Drag the dimension "MonthlyIncome"" to "Color"" and "Used_Coke_Her_Meth"" to "Size"" in the Marks area.

Step 3: Drag Depressed, Savings, and Used_Coke_Her_Meth to Filters. Edit all 3 of these so that they exclude null values. 

Step 4: In the legend area, drag and drop to rearrange Income so that "null"" is at the top and the income values increase as you read down. Rearrange the legend for Used_Coke_Her_Meth so that "yes"" is the larger circle. 





###**Scatter Plot: Days of MJ Use vs Income to Poverty Ratio**
**Interpretation**

**Government Employee**

Most people who smoked marijuana for 30 days in the past month are at the lower end of the income to poverty ratio, with a few heavy smokers in the middle and at the top. In general, more people smoked when their income:poverty ratio was < 2. Heavier smokers with a high ratio typically had a small to medium household size (~ 2-4), and few kids under 5 (usually 0). Among those with a lower ratio, more people had kids under 5, with several having 2-3. There were more small households with children as well (potentially single parents). 
![daysmjRatio_1.png](https://bitbucket.org/repo/X4GyL8/images/1020821624-daysmjRatio_1.png)

**Non-Government Employee**

We see more higher-ratio people smoking here than in the previous graph. These high-ratio people also have more kids under 5 than before. The trend remains that higher ratio people who smoked 20+ days have very small households with few kids, though. Among those with a lower ratio, people who smoke 20+ days have multiple kids under 5 and fairly large households (darker blues). Those who smoked <5 days have smaller households and only 1 kids under 5.  
![daysmjRatio_2.png](https://bitbucket.org/repo/X4GyL8/images/1496193691-daysmjRatio_2.png)

**Steps to Recreate Scatter Plot**

Step 1: Go to "Data Source" and  drag the DEMOGRAPHICS table to the **Drag tables here** area. Drag DRUGUSE to the right of DEMOGRAPHICS. Drag OCCUPATION under DRUGUSE. Make sure DEMOGRAPHICS is inner joined to both DRUGUSE and OCCUPATION. 

Step 2: Go to Sheet 1 and rename it "Days of MJ Use vs Income to Poverty Ratio". Uncheck aggregate measures. Drag the measure "Ratio_Income_Poverty"" to the columns shelf and the measure "Days_MJ_This_Month"" to the rows shelf. Drag the measure "HH_Size"" to "Color"" and "Kids_Under_5"" to "Size"" in the Marks area.

Step 3: Go to Dimensions at the top left, click the arrow, and then click "create calculated field". Name it "Employment". The calculation is **IF ([Job Descrip]) == "Working without pay at family business/farm" OR ([Job Descrip]) == "Employee of private company" OR ([Job Descrip]) == "Self-employed" THEN "Non-Government Employee" ELSEIF ([Job Descrip]) == "Null" THEN "Null" ELSE "Government Employee" END**. Hit ok and then drag the dimension "Employment" to "Pages". 

Step 3: Drag Days_MJ_This_Month and Ratio_Income_Poverty to "Filters". Edit these so that they exclude null values. 

Step 4: In the legend area, make sure that the "HH_Size"" legend has a lighter color for the lower numbers. In the Marks area, make sure the mark is a circle.  





###**Crosstabs: Drinking and Smoking Behaviors**
**Interpretation**

The following four Crosstabs are created between two binary choices: Depression/sleep depravity and alcohol/marijuana consumption, with education being the only constant variable. The value presented in the fields is calculated by taking frequency of drinking or smoking for each person, and taking the average amongst the group. Comparing the four graphs provides good information about the correlations between the three variables.

**Marijuana**

This crosstab illustrates correlations between education, sleep patterns, and any prominence to smoke marijuana. Compared with the other four graphs, this one appears the most uniform (i.e., there isn't much of a correlation between the three variables). The few outliers were because of a low count in the dataset with that group.
![smoking1.png](https://bitbucket.org/repo/X4GyL8/images/1098608600-smoking1.png)

This crosstab illustrates correlations between education, depression, and any prominence to smoke marijuana. Compared with the previous crosstab, this one is also uniform while also having lower values. Thus, based upon these two graphs, one can conclude level of education doesn't affect prominence of smoking marijuana.
![smoking2.png](https://bitbucket.org/repo/X4GyL8/images/218856868-smoking2.png)

**Alcohol**

This crosstab illustrates correlations between education, depression, and any prominence to drink alcohol. One thing is made immediately apparent in this graph by the corners - there is a direct correlation between level of depression, education, and alcohol abuse.
![alcohol1.png](https://bitbucket.org/repo/X4GyL8/images/3087322928-alcohol1.png)

This crosstab illustrates correlations between education, sleep deprivation, and any prominence to drink alcohol. Like the previous crosstab, this illustrates a strong correlation between the variables - yet, in a greater fashion. This tab shows an even stronger shift as you move down in education.
![alcohol2.png](https://bitbucket.org/repo/X4GyL8/images/1976423877-alcohol2.png)

**Steps to Recreate Crosstabs**

Step 1: Go to "Data Source" and  drag the MENTALHEALTH table to the **Drag tables here** area. Drag ALCOHOL to the right of MENTALHEALTH Drag DEMOGRAPHICS under ALCOHOL. Also drag DRUGUSE, INCOME, and OCCUPATION under DEMOGRPHICS. Make sure MENTALHEALTH is inner joined to all of the other 5 tables. 

Step 2: Go to Sheet 1 and rename it "MJ_CT1". Make sure aggregate measures is checked. Drag the dimension "Education"" to the columns shelf and the dimension "Sleep_Issues"" to the rows shelf. Also drag these two dimensions to "Filters" and exclude null values. Drag the measure "Days_MJ_This_Month"" to "Color"" and change it to AVG. Do the same for "Text". Change the title to "Smoking Patterns Based on Education and Amount of Sleep Deprivation".

Step 3: Duplicate sheet 1 and rename the result "MJ_CT2". Drag the dimension "Depressed" on top of "Sleep_Issues" in the rows and filters areas so that the latter is replaced. Change the title to "Smoking Patterns Based on Education and Level of Depression".

Step 3: Duplicate sheet 2 and rename the result "AL_CT1". Drag the dimension "Sleep_Issues" on top of "Depressed" in the rows and filters areas so that the latter is replaced. Change the color and text areas to be "AVG(Avg_Per_Day)". Change the title to "Drinking Patterns Based on Education and Depression". 

Step 4: Duplicate sheet 3 and rename the result "AL_CT2". Drag the dimension "Depressed" on top of "Sleep_Issues" in the rows and filters areas so that the latter is replaced. Change the title to "Drinking Patterns Based on Education and Amount of Sleep Deprivation".



##**Flexdashboard Storyboard Creation**
**Creating the markdown file**

Step 1: Create an R markdown file with the flexdashboard::flex_dashboard output format. 

Step 2: Add the storyboard: true option to the dashboard and runtime: shiny.

Step 3: Include a set of level 3 (###) dashboard components. Each component will be allocated it’s own frame in the storyboard, with the section title used as the navigation caption.

###**Tab 1: Bar Graph Using Shiny and Plotly HTML Widget**

![tab1.png](https://bitbucket.org/repo/X4GyL8/images/2149314934-tab1.png)

Step 1: Use required flexdashboard storyboard syntax to create a tab for the bar graph.

Step 2: Create the Shiny server script. First we need to do a SQL query within the data.frame expression. Within the SQL query, we'll create multiple joins between the following tables: **DEMOGRAPHICS, MENTALHEALTH, DRUGUSE.**


Step 3: Create a **selectInput** Shiny widget. Here we're using the attribute **marital** as input. Then, call the plotly function **plotlyOutput** passing in the parameter **barplot** and setting **width** and **height** to **"auto"**. 


Step 4: Call the **renderPlotly** function, assigning it to the **output$barplot** expression . Within the function, use the **dplyr** queries **select, filter, group_by, and summarize** to call the required attributes: **MARITAL, ANNUAL_HH_INCOME, DEPRESSED.**


 
Step 5: Call the **mapvalues** function, assigning it to the **df$scode** expression. This maps the **ANNUAL_HH_INCOME** attributes from characters to factors, which will allow us to order these attributes in the ggplot expression. 


Step 6: Create a ggplot expression. 

Step 7: Call the **ggplotly** function passing in the ggplot expression as the parameter, and pipe **layout(margin=list(b=150))** to make adjustments to the bottom margin. 

####################################################################
###**Tab 2: Box Plot Using Shiny and Plotly HTML Widget**

![tab2.png](https://bitbucket.org/repo/X4GyL8/images/953167055-tab2.png)

Step 1: Use required flexdashboard storyboard syntax to create a tab for the box plot.

Step 2: Next we create the Shiny server script. First we need to do a SQL query within the data.frame expression. Within the SQL query, we'll create multiple joins between the following tables: **DEMOGRAPHICS, MENTALHEALTH, DRUGUSE.**


Step 3: Call the **mapvalues** function, assigning it to the **df$scode** expression. This maps the **BETTER_DEAD** attributes from characters to factors, allowing us to order these attributes in the ggplot expression. 


Step 4: Create a **selectInput** Shiny widget. Here we're using the attribute **gender** as input. Then, call the plotly function **plotlyOutput** passing in the parameter **boxplot**, and setting **width** and **height** to **"auto"**.

Step 5: Call **renderPlotly** function, assigning it to the **output$boxplot** expression . Within the function, use the **dplyr** queries **select, filter, group_by, and summarize** to call the required attributes: **GENDER, RATIO_INCOME_POVERTY, BETTER_DEAD.** 

Step 6: Create a ggplot expression. 


Step 7: Call the **ggplotly** function passing in the ggplot expression as the parameter, and pipe **layout(margin=list(b=150))** to make adjustments to the bottom margin. 

###**Tab 3: 

Scatter Plot Using Shiny and Plotly HTML Widget**
![tab3.png](https://bitbucket.org/repo/X4GyL8/images/1182307736-tab3.png)

Step 1: Use required flexdashboard storyboard syntax to create a tab for the scatter plot.


Step 2: Next we create the Shiny server script. First we need to do a SQL query within the data.frame expression. Within the SQL query, we'll create multiple inner joins between the following tables: **DEMOGRAPHICS, DRUGUSE, OCCUPATION**


Step 3: Create a **selectInput** Shiny widget. Here we're using the attribute **jobdescription** as input. Then, call the plotly function **plotlyOutput** passing in the parameter **scatterplot**, and setting **width** and **height** to **"auto"**.

Step 4: Call the **renderPlotly** function, assigning it to the **output$scatterplot** expression . Within the function, use the **dplyr** queries **select, filter, and group_by** to call the required attributes: **HH_SIZE, JOB_DESCRIP, RATIO_INCOME_POVERTY, DAYS_MJ_THIS_MONTH, KIDS_UNDER_5** 


Step 5: Create a ggplot expression. 

Step 6: Call the **ggplotly** function passing in the ggplot expression as the parameter, and pipe **layout(margin=list(b=150))** to make adjustments to the bottom margin. 

###**Tab 4: Crosstab Using Shiny and Plotly HTML Widget**

![tab4.png](https://bitbucket.org/repo/X4GyL8/images/270990562-tab4.png)

Step 1: Use required flexdashboard storyboard syntax to create a tab for the crosstab.

Step 2: Next we create the user input buttons so one can select whether the crosstab looks at either Marijuana or Alcohol versus either Sleep Issues or Depression Level. These inputs are reactive and assigned the variables "intox" and "ail". Next, we call the plotly function **plotlyOutput**, passing in the parameter **crosstab**, the name of our reactive plot output. We set **width** and **height** to **"auto"**. 

Step 3: Next we do a SQL query within the data.frame expression and assign the pulled dataframe to the variable "df_CTA". Within the SQL query, we'll create multiple inner joins between the following tables: **MENTALHEALTH, ALCOHOL, DEMOGRAPHICS, DRUGUSE**. Note that v1 is the user input for "ailment" and v2 is the input for "toxicant". We select the **EDUCATION** column as well as the user selected **AVG_PER_DAY** or **DAYS_MJ_THIS_MONTH** for intoxicant/v1 and the user selected **SLEEP_ISSUES** or **DEPRESSED** for ailment/v2. We pull the average of the v2 values and round it to two decimal places before renaming it **SPEC_NUM**. Finally, we group by education and then intoxicant type. 


Step 4: Call the **renderPlotly** function, assigning it to the **output$crosstab** expression . Our ggplot expression has the name "plotST". We set the x and y axes to be discrete values and re-order the x axis values to represent increasing education levels. We also re-order the y axis values to represent decreasing frequency of depressed thoughts. We then set a title and x and y axis titles, rotate x axis names to 45 degrees, set the x variable to **EDUCATION**, the y variable to the user-selected **ISSUE**, and the label to **SPEC_NUM**. Finally, we give low **SPEC_NUM** values a fill color of blue and high values a fill color of red.

Step 5: Call the **ggplotly** function passing in the ggplot expression as the parameter, and pipe **layout(margin=list(b=150))** to make adjustments to the bottom margin. 

###**Tab 5: rBokeh Scatterplot -- Females**
![tab5.png](https://bitbucket.org/repo/X4GyL8/images/2452423678-tab5.png)
**Interpretation**

Color legend for the plot below:

**BLUE**: Divorced  |  **ORANGE**: Living with partner  |  **GREEN**: Married  |  **RED**: Never married  |  **PURPLE**: Separated  |  **BROWN**: Widowed

**Females**

Among females, most younger women have never married or live with their partner. Ages 30-70 are mostly married or divorced. There is a woman who has had 4-5 drinks 60 days in the past year. She lives with her partner, is in her mid-20s, and worked a moderate amount of hours in the past week. Other high drinkers among women (20+ days with 4-5 drinks) are a mix of married, divorced, and never married. They also worked a moderate number of hours and are all under 50 years old. Those who are divorced barely worked in the past week. One never married woman who is nearly 50 drinks a lot. Overall, married women seem to have 4-5 drinks the most often. 

.

Step 1: Use required flexdashboard storyboard syntax to create a tab for the scatter plot.

Step 2: Next we do a SQL query within the data.frame expression and assign the pulled dataframe to the variable "df4". Within the query, do inner joins between: **DEMOGRAPHICS, ALCOHOL, and OCCUPATION** tables. Subset the result to only include respondents who worked < 100 hours last week, drank 4-5 drinks during 160 or less days in the past year, and are female. Call this "female_df".


Step 3: Use the rbokeh **figure** function and **ly_points** function to create a scatterplot. Create a title and set the legend to false in the figure() area. Set the x axis to AGE, the y axis to DAYS_4TO5DRINKS, and the data source to female_df. Color by MARITAL status and size by HRS_LAST_WK.  


###**Tab 6: rBokeh Scatterplot -- Males**

**Interpretation**

**Males**

Among males, the heaviest drinker also lives with his partner. He is in his early 60s and worked a moderate amount of hours in the past week. There is a good mix of marital statuses among men who are heavier drinkers. Overall, married men most frequently have 4-5 drinks, with a large segment of the married male population from the ages for ~35 to 70 having 10+ days a year with 4-5 drinks.

Color legend for the plot below:

**BLUE**: Divorced  |  **ORANGE**: Living with partner  |  **GREEN**: Married  |  **RED**: Never married  |  **PURPLE**: Separated  |  **BROWN**: Widowed
![tab6.png](https://bitbucket.org/repo/X4GyL8/images/291875815-tab6.png)
.

Step 1: Use required flexdashboard storyboard syntax to create a tab for the scatter plot.

Step 2: Next we do a SQL query within the data.frame expression and assign the pulled dataframe to the variable "df4". Within the query, do inner joins between: **DEMOGRAPHICS, ALCOHOL, and OCCUPATION** tables. Subset the result to only include respondents who worked < 100 hours last week, drank 4-5 drinks during 160 or less days in the past year, and are male Call this "male_df".



Step 3: Use the rbokeh **figure** function and **ly_points** function to create a scatterplot. Create a title and set the legend to false in the figure() area. Set the x axis to AGE, the y axis to DAYS_4TO5DRINKS, and the data source to male_df. Color by MARITAL status and size by HRS_LAST_WK.  