require(tidyr)
require(dplyr)
require("jsonlite")
require("RCurl")
require(plyr)
require(foreign)
require(Hmisc)

# Read in original xpt file
income <- sasxport.get("INQ_H.xpt")

# Subset to less columns
income <- select(income, seqn, indfmmpi, inq244, ind247, inq132, ind235)

# Recode variables, make new renamed columns
income$ID_NUM <- income$seqn

income$PUBLIC_ASSISTANCE <- NA
income$PUBLIC_ASSISTANCE[income$inq132 == 1] <- 'Yes' 
income$PUBLIC_ASSISTANCE[income$inq132 == 2] <- 'No'     

income$MONTHLY_INCOME <- NA
income$MONTHLY_INCOME[income$ind235 == 1 | income$ind235 == 2] <- '$0 - 799'
income$MONTHLY_INCOME[income$ind235 == 3 | income$ind235 == 4] <- '$800 - 1,649'
income$MONTHLY_INCOME[income$ind235 == 5 | income$ind235 == 6] <- '$1,650 - 2,899'
income$MONTHLY_INCOME[income$ind235 == 7 | income$ind235 == 8] <- '$2,900 - 4,599'
income$MONTHLY_INCOME[income$ind235 == 9 | income$ind235 == 10] <- '$4,600 - 6,249'
income$MONTHLY_INCOME[income$ind235 == 11 | income$ind235 == 12] <- '$6,250 +'

income$MONTHLY_POVERTY_LEVEL_INDEX <- income$indfmmpi

income$SAVINGS_OVER_5K <- NA
income$SAVINGS_OVER_5K[income$inq244 == 1] <- 'Yes'
income$SAVINGS_OVER_5K[income$inq244 == 2] <- 'No'

income$SAVINGS <- NA
income$SAVINGS[income$ind247 == 1] <- 'Under $500'
income$SAVINGS[income$ind247 == 2] <- '$501 - 1,000'
income$SAVINGS[income$ind247 == 3] <- '$1,001 - 2,000'
income$SAVINGS[income$ind247 == 4] <- '$2,001 - 3,000'
income$SAVINGS[income$ind247 == 5] <- '$3,001 - 4,000'
income$SAVINGS[income$ind247 == 6] <- '$4,001 - 5,000'



# Final dataframe
df <- select(income, ID_NUM, PUBLIC_ASSISTANCE, MONTHLY_INCOME, MONTHLY_POVERTY_LEVEL_INDEX, SAVINGS_OVER_5K, SAVINGS)


names(df)

# Get rid of special characters in each column.
for(n in names(df)) {
  df[n] <- data.frame(lapply(df[n], gsub, pattern="[^ -~]",replacement= ""))
}

str(df) # Get column types to use for getting the list of measures.

measures <- c("MONTHLY_POVERTY_LEVEL_INDEX")

dimensions <- setdiff(names(df), measures)
dimensions

if( length(measures) > 1 || ! is.na(dimensions)) {
  for(d in dimensions) {
    # Get rid of " and ' in dimensions.
    df[d] <- data.frame(lapply(df[d], gsub, pattern="[\"']",replacement= ""))
    # Change & to and in dimensions.
    df[d] <- data.frame(lapply(df[d], gsub, pattern="&",replacement= " and "))
    # Change : to ; in dimensions.
    df[d] <- data.frame(lapply(df[d], gsub, pattern=":",replacement= ";"))
  }
}

# Get rid of all characters in measures except for numbers, the - sign, and period.dimensions
if( length(measures) > 1 || ! is.na(measures)) {
  for(m in measures) {
    df[m] <- data.frame(lapply(df[m], gsub, pattern="[^--.0-9]",replacement= ""))
  }
}

write.csv(df, "income.reformatted.csv", row.names=FALSE, na = "")

sql <- paste("CREATE TABLE", "INCOME", "(\n-- Change table_name to the table name you want.\n")
if( length(measures) > 1 || ! is.na(dimensions)) {
  for(d in dimensions) {
    sql <- paste(sql, paste(d, "varchar2(4000),\n"))
  }
}
if( length(measures) > 1 || ! is.na(measures)) {
  for(m in measures) {
    if(m != tail(measures, n=1)) sql <- paste(sql, paste(m, "number(38,4),\n"))
    else sql <- paste(sql, paste(m, "number(38,4)\n"))
  }
}
sql <- paste(sql, ");")
cat(sql)


#------------------------------------------------------------------------------------------------------------


# Change the USER and PASS below to be your UTEid
income_df <- data.frame(fromJSON(getURL(URLencode('oraclerest.cs.utexas.edu:5001/rest/native/?query="select * from INCOME"'),httpheader=c(DB='jdbc:oracle:thin:@aevum.cs.utexas.edu:1521/f16pdb', USER='cs329e_jmc6473', PASS='orcl_jmc6473', MODE='native_mode', MODEL='model', returnDimensions = 'False', returnFor = 'JSON'), verbose = TRUE) ))

